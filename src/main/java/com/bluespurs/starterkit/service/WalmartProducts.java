package com.bluespurs.starterkit.service;

import java.util.List;

public class WalmartProducts {
	public List<WalmartProduct> items;
	
	public WalmartProduct getBestPrice(){
		WalmartProduct bestPriced = null;
		for (WalmartProduct item : items){
			if (null == bestPriced || (float)item.salePrice < (float)bestPriced.salePrice){
				bestPriced = item;
			}
		}
		return bestPriced;
	}
}
