package com.bluespurs.starterkit.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bluespurs.starterkit.model.Product;
import com.google.gson.Gson;

public class PriceCompareServiceImpl implements PriceCompareService{

	public static final Logger log = LoggerFactory.getLogger(PriceCompareServiceImpl.class);
	private List<ProductWatch> productWatch = new ArrayList<ProductWatch>();
	private long timerFequency = 10000;
	
	Timer timer = null;
	
	@Override
	public void watchForProductPriceReduction(String searchNames, String email) {
		Product product = getBestPrice(searchNames);
		if (null != product){
			ProductWatch productToWatch = new ProductWatch();
			productToWatch.setProduct(product);
			productToWatch.setEmailAddress(email);
			productToWatch.setSearchNames(searchNames);
			productWatch.add(productToWatch);
		}
		
		if (null == timer){
			timer = new Timer();
			timer.schedule(new TimerTask() {
				@Override
	            public void run() {
					checkPrices();
	            }
			}, timerFequency, timerFequency);
		}
		
	}
	
	@Override
	public Product getBestPrice(String searchNames) {
		Product retVal = null;
		String[] searchNameArray = searchNames.split("%20");
		if (searchNameArray.length == 0)
		{
			log.warn("No search name provided, cancelling search");
			return retVal;
		}
		Product bestBuyProduct = getBestBuyPrice(searchNameArray);
		Product walmartProduct = getWalmartPrice(searchNames);
		
		if (null == bestBuyProduct && null != walmartProduct){
			retVal = walmartProduct;
		}		
		if (null == walmartProduct && null != bestBuyProduct){
			retVal = bestBuyProduct;
		}
		if (null != bestBuyProduct && null != walmartProduct){
			retVal = bestBuyProduct;
			if (walmartProduct.getBestPrice() < bestBuyProduct.getBestPrice()){
				retVal = walmartProduct;
			}				
		}		
		return retVal;
	}
	
	private Product getWalmartPrice(String searchNames){
		Product product = null;
		
		StringBuilder sb = new StringBuilder();
		sb.append("http://api.walmartlabs.com/v1/search?apiKey=rm25tyum3p9jm9x9x7zxshfa");
		sb.append("&query=").append(searchNames);			
		sb.append("&sort=price&order=asc");
		log.info("Looking up url : " + sb.toString());
		
		String rawProcductInfo = readFromURL(sb.toString());
		Gson gson = new Gson();
		WalmartProducts products = gson.fromJson(rawProcductInfo, WalmartProducts.class);	
		if (null != products){
			WalmartProduct item = products.getBestPrice();
			product = new Product(item.name, item.salePrice, "CAN", "Walmart");	
		}		
		return product;
	}
	
	private Product getBestBuyPrice(String[] searchNameArray){
		Product product = null;
		
		StringBuilder sb = new StringBuilder();
		sb.append("https://api.bestbuy.com/v1/products((");
		sb.append("search=").append(searchNameArray[0]);
		if (searchNameArray.length > 1){
			for (int i = 1; i < searchNameArray.length; i++){
				sb.append("&search=").append(searchNameArray[i]);			
			}
		}
		sb.append("))?apiKey=pfe9fpy68yg28hvvma49sc89&sort=salePrice.asc&show=name,regularPrice,salePrice&format=json");
		log.info("Looking up url : " + sb.toString());
		
		String rawProcductInfo = readFromURL(sb.toString());
		Gson gson = new Gson();
		BestBuyProducts products = gson.fromJson(rawProcductInfo, BestBuyProducts.class);	
		if (null != products){
			product = new Product(products.products.get(0).name, products.products.get(0).salePrice.floatValue(), "CAN", "BestBuy");		
		}
		return product;
	}

	private String readFromURL (String urlStr)	{
		InputStream is = null;
		StringBuilder sb = new StringBuilder();
		
		try{
			is = new URL(urlStr).openStream();
		}
		catch (IOException ioe){
			log.error("Error while reading from url '" + urlStr +"'. Error :" + ioe);
		}
		if (null != is){		
			try{
				BufferedReader br = new BufferedReader(new InputStreamReader(is));
				int ch;
				while ((ch = br.read()) != -1){
					sb.append((char) ch);
				}					
			}
			catch (IOException ioe){
				log.error("Error while parsing data. Error :" + ioe);
			}
		}
		
		return sb.toString();
	}

	private void checkPrices()
	{
		for (ProductWatch productToWatch : productWatch){
			Product current = getBestPrice(productToWatch.getSearchNames());
			if (current.getBestPrice() < productToWatch.getProduct().getBestPrice()){
				EmailService emailService = new EmailServiceImpl();
				StringBuilder sb = new StringBuilder();
				sb.append("The price of the ");
				sb.append(productToWatch.getSearchNames().replace("%20", " "));
				Product prev = productToWatch.getProduct();
				sb.append(" has dropped from ").append(prev.getBestPrice()).append(prev.getCurrency());
				sb.append(" to ").append(current.getBestPrice()).append(current.getCurrency()).append("!");
				if (!prev.getLocation().equals(current.getLocation())){
					sb.append(" Look to ").append(current.getLocation()).append(" now.");
				}
				sb.append(" Get it quick!");
				
				emailService.sendEmail(productToWatch.getEmailAddress(), "Price Drop Alert", sb.toString());
			}
		}
	}
}
