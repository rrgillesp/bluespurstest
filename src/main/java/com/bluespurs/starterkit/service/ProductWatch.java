package com.bluespurs.starterkit.service;

import com.bluespurs.starterkit.model.Product;

public class ProductWatch {
	private Product product;
	private String searchNames;
	private String emailAddress;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getSearchNames() {
		return searchNames;
	}

	public void setSearchNames(String searchNames) {
		this.searchNames = searchNames;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

}
