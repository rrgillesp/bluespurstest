package com.bluespurs.starterkit.service;

import com.bluespurs.starterkit.model.Product;

public interface PriceCompareService {
	Product getBestPrice(String searchNames);
	void watchForProductPriceReduction(String searchNames, String email);
}
