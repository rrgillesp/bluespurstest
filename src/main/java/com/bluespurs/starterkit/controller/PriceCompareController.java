package com.bluespurs.starterkit.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bluespurs.starterkit.model.Product;
import com.bluespurs.starterkit.service.PriceCompareService;
import com.bluespurs.starterkit.service.PriceCompareServiceImpl;

@RestController
public class PriceCompareController {
	public static final String INTRO = "Welcome to the Bluespurs Interview Starter Kit price comparer.";
	public static final String MONITOR = "Any reduction to the price will email to you.";
    public static final Logger log = LoggerFactory.getLogger(PriceCompareController.class);
    
	/**
     * The index page returns a simple String message to indicate if everything is working properly.
     * The method is mapped to "/PriceCompare" as a GET request.
     */
    @RequestMapping("/PriceCompare")
    public String priceCompare() {
        log.info("Visiting price Compare page");
        return INTRO;
    }
    
    @RequestMapping("/product/search")
    public Product productSearch(@RequestParam(value="name") String name) {
        log.info("Invoking product search");
        
        PriceCompareService pcService = new PriceCompareServiceImpl();
        Product product = pcService.getBestPrice(name);
        
        return product;
    }
    
    @RequestMapping("/product/monitor")
    public String productMonitor(@RequestParam(value="name") String name, @RequestParam(value="email") String email) {
        log.info("Invoking product search");
        
        PriceCompareService pcService = new PriceCompareServiceImpl();
        pcService.watchForProductPriceReduction(name, email);
        
        return MONITOR;
    }
}
