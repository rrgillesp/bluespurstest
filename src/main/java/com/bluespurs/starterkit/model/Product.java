package com.bluespurs.starterkit.model;

public class Product {
	private String productName;
	private float bestPrice;
	private String currency;
	private String location;
	
	public Product(){		
	}
	
	public Product (String name, float price, String currency, String location){
		this.productName = name;
		this.bestPrice = price;
		this.currency = currency;
		this.location = location;
	}
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public float getBestPrice() {
		return bestPrice;
	}
	public void setBestPrice(float bestPrice) {
		this.bestPrice = bestPrice;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	
}
