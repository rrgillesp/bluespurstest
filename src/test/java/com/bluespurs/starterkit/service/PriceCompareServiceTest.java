package com.bluespurs.starterkit.service;

import org.junit.Before;
import org.junit.Test;

import com.bluespurs.starterkit.service.PriceCompareService;
import com.bluespurs.starterkit.service.PriceCompareServiceImpl;

public class PriceCompareServiceTest {
	private PriceCompareService pcService;
	
	@Before
    public void setUp() {
		pcService = new PriceCompareServiceImpl();
	}
	
	@Test
    public void testGetBestPriceSingleName() throws Exception {
		pcService.getBestPrice("ipad");
	}

	@Test
    public void testGetBestPriceMultiName() throws Exception {
		pcService.getBestPrice("ipad%20mini");
	}
	
	@Test
    public void testPriceWatch() throws Exception {
		pcService.watchForProductPriceReduction("ipad%20mini", "rich.gillespie@gmail.com");
	}
}
